<?php 

class Template {

	public function __construct()
	{
		$this->ci = @get_instance();
	}

	public function render($path, $data = null)
	{
		$data['head'] = $this->ci->load->view('layout/head', $data, TRUE);
		$data['header'] = $this->ci->load->view('layout/header', $data, TRUE);
		$data['sidebar'] = $this->ci->load->view('layout/sidebar', $data, TRUE);
		$data['script'] = $this->ci->load->view('layout/script', $data, TRUE);
		$data['page'] = $this->ci->load->view($path, $data, TRUE);


		$this->ci->load->view('layout/template', $data);
	}

}

?>