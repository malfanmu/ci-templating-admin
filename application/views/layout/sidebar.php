<aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <p class="centered"><a href="<?php site_url('welcome/profile') ?>"><img src="<?php echo base_url('assets/img/ui-sam.jpg') ?>" class="img-circle" width="80"></a></p>
      <h5 class="centered">M Alfan MU</h5>
      <li class="mt">
        <a class href="<?php echo site_url('dashboard') ?>">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
          </a>
      </li>
      <li class="sub-menu">
        <a class href="<?php echo site_url('profile') ?>">
          <i class="fa fa-desktop"></i>
          <span>Profile</span>
          </a>
      </li>
      
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>