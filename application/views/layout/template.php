<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $head ?>
	<meta charset="UTF-8">
	<title><?php echo $title ?></title>
</head>
<body>
	<?php echo $header; ?>

	<?php echo $sidebar; ?>

	<p id="custom">
		<?php echo $title ?>
		<?php echo $page ?>
	</p>

	<?php echo $script; ?>
</body>
</html>