<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		$data['title'] = "Welcome <b>Profile</b> Page";
		$this->template->render('page/profile', $data);
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */

?>